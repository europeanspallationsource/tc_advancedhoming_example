﻿<?xml version="1.0" encoding="utf-8"?>
<TcPlcObject Version="1.1.0.1" ProductVersion="3.1.0.26">
  <POU Name="Test_Apolito" Id="{e6ec4fcf-811a-42e0-ba90-5bcfadce9d28}">
    <Declaration><![CDATA[PROGRAM Test_Apolito
VAR_INPUT
	AX5000_Input				: INT;
END_VAR
VAR_IN_OUT
	Axis						: ARRAY [1..4] OF AXIS_REF;

	Enable						: ARRAY [1..4] OF BOOL;
	EnablePos					: ARRAY [1..4] OF BOOL;
	EnableNeg					: ARRAY [1..4] OF BOOL;

	EL1252						: EL1252_IoInterface;	
END_VAR
VAR
	CalibrationCam				: ARRAY [1..4] OF BOOL;
	SoftEndPos,SoftEndNeg:BOOL;
	eTestCase					: E_TestCases := eTestCase_HomeBlock;
	
	StartHoming					: BOOL;
	UseLagBased					: BOOL;

	AxEnable					: BOOL := TRUE;

	LimitSwitchMode				: MC_Switch_Mode;					(* E *)
	LimitSwitchSignal 			: MC_Ref_Signal_Ref;				(* V - vgl. MC_REF_SIGNAL_REF in PLCOpen*)

	AbsoluteSwitchMode			: MC_Switch_Mode;	
	AbsoluteSwitchSignal 		: MC_Ref_Signal_Ref;				(* V - vgl. MC_REF_SIGNAL_REF in PLCOpen*)
	XFCAbsoluteSwitchSignal 	: XFC_Ref_Signal_Ref;				(* V - vgl. MC_REF_SIGNAL_REF in PLCOpen*)

	TorqueLimit					: LREAL := 40.0;
	TorqueTolerance				: LREAL := 5.0;
	LagLimit					: LREAL;
	DetectionVelocityLimit		: LREAL := 2.0;
	DetectionVelocityTime		: TIME := T#200MS;

	BufferMode					: MC_BufferMode;
	Direction					: MC_Home_Direction := mcPositiveDirection;(*MC_Negative_Direction*)

	HomeBlockDetection			: FB_HomeBlock_Example;
	HomeBlockLagBasedDetection	: FB_HomeBlockLagBased_Example;
	HomeLimitSwitchDetection	: FB_HomeLimitSwitch_Example;
	
	HomeAbsoluteSwitchDetection	: FB_HomeAbsoluteSwitch_Example;
	XFCHomeAbsoluteSwitchDetection	: XFC_HomeAbsoluteSwitch_Example;

	ReadApplicationRequest		: MC_ReadApplicationRequest;
	ApplicationRequest			: ST_NcApplicationRequest;

	(* outputs of FB_HomeBlock *)
	HomeDone					: BOOL;
	HomeBusy					: BOOL;
	HomeActive					: BOOL;
	HomeCommandAborted			: BOOL;
	HomeError					: BOOL;
	HomeErrorID					: UDINT;

	StartParameterHandling		: BOOL;	
	StepHomingParameter			: MC_StepHomingParameter;
	Parameter					: MC_HomingParameter;

//	HOMINGPARAMETERCTRLMODE_READ
//	HOMINGPARAMETERCTRLMODE_PREPARE	
//	HOMINGPARAMETERCTRLMODE_RESTORE
	Mode						: MC_HomingParameterCtrlMode;
	
	TwinCAT						: UDINT;
	Version						: UDINT;
	Build						: UDINT;
	TwinCAT2						: UDINT;
	Version2						: UDINT;
	Build2						: UDINT;
END_VAR]]></Declaration>
    <Implementation>
      <ST><![CDATA[TwinCAT 	:= SHR(TcMcGlobal.NcDeviceInfoNcVersion,24);
Version 	:= SHR(TcMcGlobal.NcDeviceInfoNcVersion AND 16#00FF0000,16);
Build 		:= TcMcGlobal.NcDeviceInfoNcVersion AND 16#0000FFFF;

TwinCAT2 	:= SHR(16#03010FA2,24);
Version2 	:= SHR(16#03010FA2 AND 16#00FF0000,16);
Build2 		:= 16#03010FA2 AND 16#0000FFFF;

(* Test Tasterfeld Öffner *)
	Enable[1]				:= AxEnable;
	EnablePos[1]			:= AX5000_Input.0 OR eTestCase = eTestCase_HomeBlock;// OR TRUE;
	EnableNeg[1]			:= AX5000_Input.1 OR eTestCase = eTestCase_HomeBlock;// OR TRUE;

	SoftEndPos:=AX5000_Input.0;
	SoftEndNeg:=AX5000_Input.1;
	
(* Test Tasterfeld Schliesser *)
	CalibrationCam[1]		:= AX5000_Input.3;

	
CASE eTestCase OF
	eTestCase_HomeBlock:
		ActHomeBlock();
	eTestCase_HomeLimitSwitch:
		ActHomeLimitSwitch();
	eTestCase_HomeAbsoluteSwitch:
		ActHomeAbsoluteSwitch();
//	eTestCase_XFCHomeAbsoluteSwitch:
//		ActXFCHomeAbsoluteSwitch();	
	eTestCase_HomeReferencePulse:
END_CASE
]]></ST>
    </Implementation>
    <Action Name="ActHomeLimitSwitch" Id="{29368cc1-ac2d-48f0-bb03-0887792ffc46}">
      <Implementation>
        <ST><![CDATA[LimitSwitchSignal.Level				:= SEL(Direction = mcPositiveDirection, NOT AX5000_Input.1, NOT AX5000_Input.0);
LimitSwitchSignal.SignalSource		:= SignalSource_Default;
LimitSwitchSignal.TouchProbe		:= PlcEvent;

HomeLimitSwitchDetection(
	Axis					:= Axis[1], 
	Execute					:= StartHoming, 
	StepBackDistance		:= 0.0, 
	Direction				:= Direction, 
	LimitSwitchMode			:= LimitSwitchMode, 
	LimitSwitchSignal		:= LimitSwitchSignal, 
	Velocity				:= 200.0, 
	Acceleration			:= , 
	Deceleration			:= , 
	Jerk					:= ,
	SetPosition				:= 500.0, 
	TorqueLimit				:= TorqueLimit,
	TimeLimit				:= T#10S, 
	DistanceLimit			:= 10000.0, 
	Options					:= , 
	(* outputs *)
	Done 					=> HomeDone,
	Busy 					=> HomeBusy,
	Active				 	=> HomeActive,
	CommandAborted 			=> HomeCommandAborted,
	Error 					=> HomeError,
	ErrorID 				=> HomeErrorID);
	
]]></ST>
      </Implementation>
    </Action>
    <Action Name="ActXFCHomeAbsoluteSwitch" Id="{a4c59be2-a7fa-4ee5-8344-348f6650211e}">
      <Implementation>
        <ST><![CDATA[XFCAbsoluteSwitchSignal.Level					:= EL1252.Input;
XFCAbsoluteSwitchSignal.TimeStampRisingEdge		:= EL1252.TimeStampPos.dwLowPart;
XFCAbsoluteSwitchSignal.TimeStampFallingEdge	:= EL1252.TimeStampNeg.dwLowPart;

XFCHomeAbsoluteSwitchDetection(
	Axis					:= Axis[1], 
	Execute					:= StartHoming, 
	StepBackDistance		:= 0.0, 
	Direction				:= Direction, 
	SwitchMode				:= AbsoluteSwitchMode, 
	ReferenceSignal			:= XFCAbsoluteSwitchSignal, 
	PositiveLimitSwitch		:= NOT AX5000_Input.0, 
	NegativeLimitSwitch		:= NOT AX5000_Input.1, 
	Velocity				:= 20.0, 
	Acceleration			:= , 
	Deceleration			:= , 
	Jerk					:= ,
	SetPosition				:= 500.0, 
	TorqueLimit				:= TorqueLimit,
	TimeLimit				:= T#60S, 
	DistanceLimit			:= 10000.0, 
	BufferMode				:= , 
	Options					:= , 
	(* outputs *)
	Done 					=> HomeDone,
	Busy 					=> HomeBusy,
	Active				 	=> HomeActive,
	CommandAborted 			=> HomeCommandAborted,
	Error 					=> HomeError,
	ErrorID 				=> HomeErrorID);	
]]></ST>
      </Implementation>
      <ObjectProperties />
    </Action>
    <Action Name="ActHomeBlock" Id="{ae2a3de7-0397-4989-bc60-9b8a5cc37bda}">
      <Implementation>
        <ST><![CDATA[IF NOT UseLagBased THEN		
//	TorqueLimit := 0.3;
	HomeBlockDetection(
		Axis					:= Axis[1], 
		Execute					:= StartHoming, 
		StepBackDistance		:= 50.0, 
		Direction				:= Direction, 
		Velocity				:= 50.0, 
		Acceleration			:= , 
		Deceleration			:= , 
		Jerk					:= ,
 		SetPosition				:= 500.0, 
		DetectionVelocityLimit	:= DetectionVelocityLimit, 
		DetectionVelocityTime	:= DetectionVelocityTime, 
		TimeLimit				:= T#2M, 
		DistanceLimit			:= 10000.0, 
		TorqueLimit				:= TorqueLimit,
		TorqueTolerance			:= TorqueTolerance,
		Options					:=,
		(* outputs *)
		Done 					=> HomeDone,
		Busy 					=> HomeBusy,
		Active				 	=> HomeActive,
		CommandAborted 			=> HomeCommandAborted,
		Error 					=> HomeError,
		ErrorID 				=> HomeErrorID);
ELSE
	HomeBlockLagBasedDetection(
		Axis					:= Axis[1], 
		Execute					:= StartHoming, 
		StepBackDistance		:= 50.0, 
		Direction				:= Direction, 
		Velocity				:= 50.0, 
		Acceleration			:= , 
		Deceleration			:= , 
		Jerk					:= , 
		SetPosition				:= 500.0, 
		DetectionVelocityLimit	:= 1.0, 
		DetectionVelocityTime	:= T#50MS, 
		TimeLimit				:= T#2M, 
		DistanceLimit			:= 10000.0, 
		TorqueLimit				:= TorqueLimit,
		LagLimit				:= LagLimit,
		Options					:=,
		(* outputs *)
		Done 					=> HomeDone,
		Busy 					=> HomeBusy,
		Active				 	=> HomeActive,
		CommandAborted 			=> HomeCommandAborted,
		Error 					=> HomeError,
		ErrorID 				=> HomeErrorID);
END_IF		
]]></ST>
      </Implementation>
    </Action>
    <Action Name="ActHomeAbsoluteSwitch" Id="{b72a2f9d-522b-4c5f-b01c-7990ed57d198}">
      <Implementation>
        <ST><![CDATA[AbsoluteSwitchSignal.Level				:= EL1252.Input;
AbsoluteSwitchSignal.SignalSource		:= SignalSource_Default;
AbsoluteSwitchSignal.TouchProbe			:= PlcEvent;

HomeAbsoluteSwitchDetection(
	Axis					:= Axis[1], 
	Execute					:= StartHoming, 
	StepBackDistance		:= 0.0, 
	Direction				:= Direction, 
	SwitchMode				:= AbsoluteSwitchMode, 
	ReferenceSignal			:= AbsoluteSwitchSignal, 
	PositiveLimitSwitch		:= NOT AX5000_Input.0, 
	NegativeLimitSwitch		:= NOT AX5000_Input.1, 
	Velocity				:= 20.0, 
	Acceleration			:= , 
	Deceleration			:= , 
	Jerk					:= ,
	SetPosition				:= 500.0, 
	TorqueLimit				:= TorqueLimit,
	TimeLimit				:= T#60S, 
	DistanceLimit			:= 10000.0, 
	BufferMode				:= , 
	Options					:= , 
	(* outputs *)
	Done 					=> HomeDone,
	Busy 					=> HomeBusy,
	Active				 	=> HomeActive,
	CommandAborted 			=> HomeCommandAborted,
	Error 					=> HomeError,
	ErrorID 				=> HomeErrorID);	
]]></ST>
      </Implementation>
    </Action>
    <ObjectProperties />
  </POU>
</TcPlcObject>